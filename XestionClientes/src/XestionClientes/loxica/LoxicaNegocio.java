/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XestionClientes.loxica;

import XestionClientes.dto.Cliente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pablo
 */
public class LoxicaNegocio {
    private static List<Cliente> listaClientes = new ArrayList<>();

    public static void engadirCliente(Cliente cliente) {
        listaClientes.add(cliente);
    }

    public static List<Cliente> getListaClientes() {
        return listaClientes;
    }
    
    public static void modificarCliente(int posicion, Cliente cliente) {
        listaClientes.set(posicion, cliente);
    }
    
}
