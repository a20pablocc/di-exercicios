/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XestionClientes.gui.tableModel;

import XestionClientes.dto.Cliente;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Pablo
 */
public class ClientesTableModel extends GenericDomainTableModel<Cliente>{
    
  @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return String.class;
            case 2: return Date.class;
            case 3: return String.class;
        }
        throw new ArrayIndexOutOfBoundsException(columnIndex);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cliente cliente = getDomainObject(rowIndex);
        switch(columnIndex) {
            case 0: return cliente.getNome();
            case 1: return cliente.getApelidos();
            case 2: return cliente.getDataAlta();
            case 3: return cliente.getProvincia();
                default: throw new ArrayIndexOutOfBoundsException(columnIndex);
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Cliente cliente= getDomainObject(rowIndex);
        switch(columnIndex) {
            case 0: cliente.setNome((String)aValue); break;
            case 1: cliente.setApelidos((String)aValue); break;
            case 2: cliente.setDataAlta((Date)aValue); break;
            case 3: cliente.setProvincia((String)aValue); break;
                default: throw new ArrayIndexOutOfBoundsException(columnIndex);
        }
        notifyTableCellUpdated(rowIndex, columnIndex); // NO olvidar!!!
    }

    public ClientesTableModel(List columnIdentifiers) {
        super(columnIdentifiers);
    }
    
    
}

