/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.validation.api.builtin.stringvalidation;

import org.netbeans.validation.api.Problems;
import org.openide.util.NbBundle;

/**
 *
 * @author Pablo
 */
public class ValidacionMaiusculas extends StringValidator{

    @Override
    public void validate(Problems problemas, String nomeComponente, String texto) {
        if ((!"".equals(texto)) && !Character.isUpperCase(texto.charAt(0))) {
        String msx = NbBundle.getMessage(this.getClass(),"MSG_MAIUSCULAS", nomeComponente);
        problemas.add(msx);
        }
    }
}
