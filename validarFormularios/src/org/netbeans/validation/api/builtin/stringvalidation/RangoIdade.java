/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.validation.api.builtin.stringvalidation;

import static java.lang.Integer.parseInt;
import org.netbeans.validation.api.Problems;
import org.openide.util.NbBundle;

/**
 *
 * @author Pablo
 */
public class RangoIdade extends StringValidator{
    @Override
    public void validate(Problems problemas, String nomeComponente, String texto) {
        if (!"".equals(texto)&&parseInt(texto)<=18 && parseInt(texto)>=65) {
            String msx = NbBundle.getMessage(this.getClass(),"MSG_RANGOIDADE", nomeComponente);
            problemas.add(msx);
        }
    }
}
